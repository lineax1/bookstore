﻿using BookStore.BusinessLogicLayer.Services.Interfaces;
using BookStore.BusinessLogicLayer.Views.AccountViews;
using BookStore.DataAccessLayer.Models;
using BookStore.Shared;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.BusinessLogicLayer.Services
{
    public class AccountService : IAccountService
    {
        //private readonly SignInManager<IdentityUser> _signInManager;
        //private readonly UserManager<IdentityUser> _userManager;
        private readonly IConfiguration _configuration;

        public AccountService(/*SignInManager<IdentityUser> signManager, UserManager<IdentityUser> userManager,*/ IConfiguration configuration)
        {
            //_signInManager = signManager;
            //_userManager = userManager;
            _configuration = configuration;
        }
        //        public async Task<User> Register(RegisterAccountView model)
        //        {
        //        var user = new User
        //        {
        //            UserName = model.Email,
        //            Email = model.Email,
        //            FirstName = model.FirstName,
        //            LastName = model.LastName,
        //            Role = "user"
        //        };
        //        var result = await _userManager.CreateAsync(user, model.Password);
        //            if (!result.Succeeded)
        //            {
        //                var errors = result.Errors.ToString();
        //                throw new BusinessLogicException(errors);
        //    }
        //    await _signInManager.SignInAsync(user, false);
        //            return user;
        //        }
        //public async Task<object> Login([FromBody] LoginAccountView model)
        //        {
        //            var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, false, false);

        //            if (result.Succeeded)
        //            {
        //                var appUser = _userManager.Users.SingleOrDefault(r => r.Email == model.Email);
        //                return  GenerateJwtToken(model.Email, appUser);
        //            }
        //            throw new ApplicationException("Invalid_Login_Attemp");
        //        }
        //создание jwt Токена
        public string GenerateJwtToken(string email, User user)
        {
            //var temp = _configuration["JwtKey_Ne_Pridumal"];
            var claims = new List<Claim>
                {
                    new Claim(JwtRegisteredClaimNames.Sub, email),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(ClaimTypes.NameIdentifier, user.Id)
                };

            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_configuration["JwtKey_Ne_Pridumal"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now;

            var token = new JwtSecurityToken(
               _configuration["JwtIssuer"],
               _configuration["JwtAudience"],
               claims: claims,
               expires: expires.Add(TimeSpan.FromMinutes(10)),
               signingCredentials: creds
                ) ;
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}

