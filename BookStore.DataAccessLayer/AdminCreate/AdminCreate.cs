﻿using BookStore.DataAccessLayer.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BookStore.DataAccessLayer.AdminCreate
{
    public class AdminCreate
    {
        public static async void DbInitializer(UserManager<User> userManager)
        {
            if (userManager.Users.Any())
            {
                return;
            }
            var user = new User()
            {
                UserName = "admin1402@gmail.com",
                Email = "admin1402@gmail.com",
                Role = "admin"
            };
            string password = "12345678";
            await userManager.CreateAsync(user, password);
        }
    }
}
