﻿using BookStore.DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repository.Interfaces
{
   public interface IBookAuthor
    {
        Task<Book> GetBookTitle(string title);
        Task<Book> GetBookId(int id);
        Task<IEnumerable<Book>> GetAllBook();
        Task<Author> GetAuthorName(string name);
        Task<Author> GetAllAuthor();
        Task<Author> GetAuthorId(int id);
    }
}
