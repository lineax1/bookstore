﻿using BookStore.DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repository.Interfaces
{
   public interface IBookMagazine
    {
        Task<Book> GetBookTitle(string title);
        Task<Book> GetBookId(int id);
        Task<IEnumerable<Book>> GetAllBook();
        Task<Magazine> GetMagazineTitle(string Name);
        Task<IEnumerable<Magazine>> GetAllMagazine();
        Task<Magazine> GetMagazineId(int id);
    }
}
