﻿using BookStore.DataAccessLayer.Models;
using BookStore.DataAccessLayer.Repository.Interfaces;
using BookStore.Shared;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repository
{
    public class BookMagazineRepository : IBookMagazine
    {
        private readonly AppSettings _appsettings;

        public BookMagazineRepository(AppSettings appSettings)
        {
            _appsettings = appSettings;
        }
        public async Task<Book> GetBookId(int id)
        {
            using (IDbConnection db = new SqlConnection(_appsettings.ConnectionString))
            {
                return await db.QuerySingleAsync<Book>($"SELECT * FROM Books WHERE Id = {id}");
            }
        }
        public async Task<Book> GetBookTitle(string title)
        {
            using (IDbConnection db = new SqlConnection(_appsettings.ConnectionString))
            {
                return await db.QuerySingleAsync<Book>($"SELECT * FROM Books WHERE Title = @Title", new { Title = title });
            }
        }
        public async Task<IEnumerable<Book>> GetAllBook()
        {
            using (IDbConnection db = new SqlConnection(_appsettings.ConnectionString))
            {
                return (await db.QueryAsync<Book>($"SELECT * FROM Books")).ToList();
            }
        }
        public async Task<Magazine> GetMagazineId(int id)
        {
            using (IDbConnection db = new SqlConnection(_appsettings.ConnectionString))
            {
                return await db.QuerySingleAsync<Magazine>($"SELECT * FROM Magazines WHERE Id = {id}");
            }
        }
        public async Task<Magazine> GetMagazineTitle(string title)
        {
            using (IDbConnection db = new SqlConnection(_appsettings.ConnectionString))
            {
                return await db.QuerySingleAsync<Magazine>($"SELECT * FROM Magazines WHERE Title = @Title", new { Title = title });
            }
        }
        public async Task<IEnumerable<Magazine>> GetAllMagazine()
        {
            using (IDbConnection db = new SqlConnection(_appsettings.ConnectionString))
            {
                return (await db.QueryAsync<Magazine>($"SELECT * FROM Magazines")).ToList();
            }
        }
    }
}

