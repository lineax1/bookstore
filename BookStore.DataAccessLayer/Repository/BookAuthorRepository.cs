﻿using BookStore.DataAccessLayer.Models;
using BookStore.Shared;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataAccessLayer.Repository
{
   public class BookAuthorRepository
    {
        private readonly AppSettings _appsettings;

        public BookAuthorRepository(AppSettings appSettings)
        {
            _appsettings = appSettings;
        }

        public async Task<Book> GetBookId(int id)
        {
            using (IDbConnection db = new SqlConnection(_appsettings.ConnectionString))
            {
                return await db.QuerySingleAsync<Book>($"SELECT * FROM Books WHERE Id = {id}");
            }
        }
        public async Task<Book> GetBookTitle(string title)
        {
            using (IDbConnection db = new SqlConnection(_appsettings.ConnectionString))
            {
                return await db.QuerySingleAsync<Book>($"SELECT * FROM Books WHERE Title = @Title", new { Title = title });
            }
        }
        public async Task<IEnumerable<Book>> GetAllBook()
        {
            using (IDbConnection db = new SqlConnection(_appsettings.ConnectionString))
            {
                return (await db.QueryAsync<Book>($"SELECT * FROM Authors")).ToList();
            }
        }
        public async Task<Author> GetAuthorName(string name)
        {
            using (IDbConnection db = new SqlConnection(_appsettings.ConnectionString))
            {
                return await db.QuerySingleAsync<Author>($"SELECT * FROM Authors WHERE Name = @Name", new { Name = name });
            }
        }
        public async Task<Author> GerAuthorId(int id)
        {
            using (IDbConnection db = new SqlConnection(_appsettings.ConnectionString))
            {
                return await db.QuerySingleAsync<Author>($"SELECT * FROM Authors WHERE id = {id}");
            }
        }
        public async Task<IEnumerable<Author>> GetAllAuthor()
        {
            using (IDbConnection db = new SqlConnection(_appsettings.ConnectionString))
            {
                return (await db.QueryAsync<Author>($"SELECT * FROM Authors")).ToList();
            }
        }
    }
}
