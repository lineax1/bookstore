import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { LoginService } from "src/app/services/authentication/login.service";
import { UserService } from "src/app/services/authentication/user.service";
// import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UserRegistration } from '../../models/authentication/user.registration';


@Component({
  selector: 'register',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
})
export class RegistrationComponent implements OnInit {
  // зregisterForm: FormGroup;
  public errors: string;
  public isRequesting: boolean;
  public submitted: boolean = false;

  constructor(
    public router: Router,
    public loginService: LoginService,
    // public formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    // this.registerForm = this.formBuilder.group({
    //   firstName: ["", Validators.required],
    //   lastName: ["", Validators.required],
    //   username: ["", Validators.required],
    //   password: ["", [Validators.required, Validators.minLength(6)]]
    // });
  }

  public registerUser({ value, valid }) {
    debugger;
    if (!valid) {
      return;
    }

    this.submitted = true;
    this.isRequesting = true;
    this.errors = "";

    this.loginService.register(value.email, value.password, value.firstName, value.lastName)
    // .finally(() => this.isRequesting = false)
    .subscribe(
        result => {
            if (result) {
                this.router.navigate(['/login'], { queryParams: { brandNew: true, email: value.email } });
            }
        },
        errors => this.errors = errors);
  }
}
