import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Identifiers, identifierModuleUrl } from '@angular/compiler';
import { GetAuthorListView } from 'src/app/models/author/get.author.list.view';
import { AuthorService } from 'src/app/services/author.service';



@Component({
  selector: "author",
  templateUrl: "./author.component.html",
  styleUrls: ["./author.component.css"]
})
export class AuthorComponent implements OnInit {
  public authorList: GetAuthorListView={authors:[]};
constructor(public authorService: AuthorService  ){
  
}
  ngOnInit() {
    this.getauthors();
  }
  public getauthors()
  {
   this.authorService.get().subscribe(res => {
this.authorList = res
    })
  }

 
  
}
