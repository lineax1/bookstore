import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute} from "@angular/router";
import { FormBuilder, FormGroup, Validator, Validators } from "@angular/forms"
import { LoginService } from "src/app/services/authentication/login.service"
import { first } from "rxjs/operators";

@Component({
  selector: 'app-login',
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})

export class LoginComponent implements OnInit {
loginForm: FormGroup;
loading = false;
submited = false;
returnUrl: string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private loginService: LoginService
    ) {
        // redirect to home if already logged in
     if(this.loginService.currentUserValue)
     {this.router.navigate(['/']);}
       }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
            // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
      // convenience getter for easy access to form fields
  get f() {return this.loginForm.controls;}

  onSubmit() {
    this.submited = true;
    
    if(this.loginForm.invalid){
      return;
    }

    this.loading = true;
    this.loginService.login(this.f.username.value, this.f.password.value)
    .pipe(first())
    .subscribe(
      data => {
        this.router.navigate([this.returnUrl]);
      },
      error => {
        this.loading = false;
      });
    


  }
  
}
