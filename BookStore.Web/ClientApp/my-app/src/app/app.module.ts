import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { Routes, RouterModule } from "@angular/router";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HttpClientModule } from "@angular/common/http";
import { AuthorComponent } from "./components/author/author.component";
import { AuthorService } from "./services/author.service";
import { RegistrationComponent } from "./components/registration/registration.component";
import { LoginService } from "./services/authentication/login.service";
import { UserComponent } from "./components/user/user.component";
import { LoginComponent } from "./components/login/login.component";
import { FormsModule } from "@angular/forms";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

@NgModule({
  declarations: [
    AppComponent,
    AuthorComponent,
    RegistrationComponent,
    UserComponent,
    LoginComponent
  ],
  imports: [
    NgbAlertModule,
    NgbModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: "", redirectTo: "author", pathMatch: "full" },
      { path: "author", component: AuthorComponent },
      { path: "registration", component: RegistrationComponent },
      { path: "login", component: LoginComponent },
      { path: "**", redirectTo: "/" }
    ]),
    BrowserAnimationsModule
  ],
  providers: [AuthorService, LoginService],
  bootstrap: [AppComponent]
})
export class AppModule {}
