import { GetAuthorView } from './get.author.view';

export interface GetAuthorListView{
    
     authors: Array<GetAuthorView>;
}