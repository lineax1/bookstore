import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { GetAuthorListView } from "../models/author/get.author.list.view";
@Injectable()
export class AuthorService {
  private url: string = "http://localhost:56227/api/author/getall";
  constructor(private http: HttpClient) {}
  public get() {
    return this.http.get<GetAuthorListView>(this.url);
  }
}
