import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Observable } from 'rxjs';
import { UserRegistration } from "src/app/models/authentication/user.registration";
import { User } from "src/app/models/authentication/user";
import { isError } from "util";

@Injectable()
export class LoginService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem("currentUser")));
    this.currentUser = this.currentUserSubject.asObservable();     
    }   

    public get currentUserValue(): User{
        return this.currentUserSubject.value;
    }

  public register(
    email: string,
    password: string,
    firstname: string,
    lastname: string
  ) {
    let user = new UserRegistration();
    user.email = email;
    user.password = password;
    user.firstName = firstname;
    user.lastName = lastname;

    return this.http.post("http://localhost:56227/api/account/register", user);
  }
  public login(userName: string, password:string) {
      let user = new User();
      user.email = userName;
      user.password = password;
    if (user && user.token) {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
    return this.http.post("http://localhost:56227/api/account/login", user, {
      responseType: "text"
    });
  }
}
public logout() {
    localStorage.removeItem("currentUser");
    this.currentUserSubject.next(null);
  }
}
